import os
from flask import Blueprint, render_template
from flask_login import login_required

# Init Blueprint
dashboard_admin_blueprint = Blueprint('dashboard_admin', __name__, template_folder='templates')


@dashboard_admin_blueprint.route('/')
@login_required
def index():
    env = os.getenv('FLASK_CONFIG') or 'default'
    return render_template('admin/index.html', js='pages/index', env=env)
