from datetime import datetime
from flask import Blueprint, render_template, redirect, url_for, flash, abort
from flask_login import login_required
from app import db
from app.models.User import User
from app.models.Role import Role
from .forms import AddUserForm, EditUserForm

# Init Blueprint
users_admin_blueprint = Blueprint('users_admin', __name__, template_folder='templates')


@users_admin_blueprint.route('/')
@login_required
def users():
    all_users = User.query.all()
    roles = Role.query.all()
    return render_template('admin/users/index.html', users=all_users, roles=roles)


@users_admin_blueprint.route('/user/<int:user_id>', methods=['GET', 'POST'])
@login_required
def edit_user(user_id):
    user = User.query.filter_by(id=user_id).first()
    form = EditUserForm(user=user)

    if user is None:
        abort(404)

    if form.validate_on_submit():
        user.email = form.email.data
        user.username = form.username.data
        user.role_id = form.role.data

        if form.password.data != "":
            user.password = form.password.data

        db.session.add(user)
        flash('"{0}" has been saved'.format(user.username))

        return redirect(url_for('.users'))

    form.email.data = user.email
    form.username.data = user.username
    form.role.data = user.role_id

    return render_template('admin/users/edit_user.html', form=form, user=user)


@users_admin_blueprint.route('/user/new', methods=['GET', 'POST'])
@login_required
def add_user():
    form = AddUserForm()

    if form.validate_on_submit():
        user = User()

        user.email = form.email.data
        user.username = form.username.data
        user.password = form.password.data
        user.role_id = form.role.data
        user.created_on = datetime.utcnow()

        db.session.add(user)
        flash('"{0}" has been added'.format(user.username))

        return redirect(url_for('.users'))

    return render_template('admin/users/add_user.html', form=form)


@users_admin_blueprint.route('/user/<int:user_id>/delete', methods=['GET', 'POST'])
@login_required
def delete_user(user_id):
    user = User.query.filter_by(id=user_id).first()

    if user is not None:
        db.session.delete(user)

        flash('"{0}" has been deleted.'.format(user.username))
        return redirect(url_for('.users'))

    flash('User does not exist')
    return redirect(url_for('.users'))
