from .auth import auth_blueprint
from .blog import blog_admin_blueprint
from .dashboard import dashboard_admin_blueprint
from .files import files_admin_blueprint
from .menus import menus_admin_blueprint
from .pages import pages_admin_blueprint
from .settings import settings_admin_blueprint
from .users import users_admin_blueprint
from flask import Blueprint


def register_blueprints(flask_app, prefix='admin'):
    """Register Admin Blueprints"""
    flask_app.register_blueprint(auth_blueprint, url_prefix=f'/{prefix}')
    flask_app.register_blueprint(
        blog_admin_blueprint, url_prefix=f'/{prefix}/blog')
    flask_app.register_blueprint(
        dashboard_admin_blueprint, url_prefix=f'/{prefix}')
    flask_app.register_blueprint(
        files_admin_blueprint, url_prefix=f'/{prefix}/files')
    flask_app.register_blueprint(
        menus_admin_blueprint, url_prefix=f'/{prefix}/menus')
    flask_app.register_blueprint(
        pages_admin_blueprint, url_prefix=f'/{prefix}/pages')
    flask_app.register_blueprint(
        settings_admin_blueprint, url_prefix=f'/{prefix}/settings')
    flask_app.register_blueprint(
        users_admin_blueprint, url_prefix=f'/{prefix}/users')

    return flask_app
