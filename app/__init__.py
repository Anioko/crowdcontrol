import os
from flask import Flask
from flask_bootstrap import Bootstrap
from flask_mail import Mail
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from config import config
from app.jinja_filters import format_datetime

bootstrap = Bootstrap()
mail = Mail()
moment = Moment()
db = SQLAlchemy()

login_manager = LoginManager()


def create_app(config_name):
    the_app = Flask(__name__)
    the_app.config.from_object(config[config_name])
    config[config_name].init_app(the_app)

    bootstrap.init_app(the_app)
    mail.init_app(the_app)
    moment.init_app(the_app)
    db.init_app(the_app)

    # Let's set up our login handlers
    import app.login_handlers
    login_manager.session_protection = 'strong'
    login_manager.login_view = 'auth.login'
    login_manager.init_app(the_app)

    the_app.jinja_env.filters['datetime'] = format_datetime

    from app.site import register_blueprints as register_site_blueprints
    the_app = register_site_blueprints(the_app)

    from app.admin import register_blueprints as register_admin_blueprints
    the_app = register_admin_blueprints(the_app)

    return the_app


app = create_app(os.getenv('FLASK_CONFIG') or 'default')

if __name__ == "__main__":
    app.run()
