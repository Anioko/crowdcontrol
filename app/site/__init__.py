from .blog import blog_blueprint
from .pages import pages_blueprint


def register_blueprints(flask_app, prefix=''):
    """Register Site Blueprints"""
    flask_app.register_blueprint(blog_blueprint, url_prefix='/blog')
    flask_app.register_blueprint(pages_blueprint, url_prefix='/')

    return flask_app
